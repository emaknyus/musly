# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/emaknyus/Workspaces/proyek/musly/musly/collectionfile.cpp" "/home/emaknyus/Workspaces/proyek/musly/build/musly/CMakeFiles/musly.dir/collectionfile.cpp.o"
  "/home/emaknyus/Workspaces/proyek/musly/musly/fileiterator.cpp" "/home/emaknyus/Workspaces/proyek/musly/build/musly/CMakeFiles/musly.dir/fileiterator.cpp.o"
  "/home/emaknyus/Workspaces/proyek/musly/musly/main.cpp" "/home/emaknyus/Workspaces/proyek/musly/build/musly/CMakeFiles/musly.dir/main.cpp.o"
  "/home/emaknyus/Workspaces/proyek/musly/musly/programoptions.cpp" "/home/emaknyus/Workspaces/proyek/musly/build/musly/CMakeFiles/musly.dir/programoptions.cpp.o"
  "/home/emaknyus/Workspaces/proyek/musly/musly/tools.cpp" "/home/emaknyus/Workspaces/proyek/musly/build/musly/CMakeFiles/musly.dir/tools.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "MUSLY_VERSION=\"0.2\""
  "NDEBUG"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/emaknyus/Workspaces/proyek/musly/build/libmusly/CMakeFiles/libmusly.dir/DependInfo.cmake"
  "/home/emaknyus/Workspaces/proyek/musly/build/libmusly/libresample/CMakeFiles/musly_resample.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "."
  "../include"
  "/usr/include/eigen3"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
